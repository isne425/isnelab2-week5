#include <queue>
#include <stack>
using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node {
public:
	Node() { left = right = NULL; }
	Node(const T& el, Node *l = 0, Node *r = 0) {
		key = el; left = l; right = r;
	}
	T key;
	Node *left, *right;
};

template<class T>
class Tree {
public:
	Tree() { root = 0; }
	~Tree() { clear(); }
	void clear() { clear(root); root = 0; }
	bool isEmpty() { return root == 0; }
	void inorder() { inorder(root); }
	void insert(const T& el);
	void deleteNode(Node<T> *& node);
	void showinorder() { showinorder(root); }

protected:
	Node<T> *root;

	void clear(Node<T> *p);
	void inorder(Node<T> *p);
	void showinorder(Node<T> *p);

};

template<class T> 
void Tree<T>::clear(Node<T> *p)
{
	if (p != 0) {
		clear(p->left);
		clear(p->right);
		delete p;
	}
}

template<class T>
void Tree<T>::inorder(Node<T> *p) {
	// to do here
	if (root != NULL)
	{
		if (p->left != NULL) {
			showinorder(p->left);
		}
		cout << p->key << " " << endl;
		if (p->right != NULL) {
			showinorder(p->right);

		}
	}
	else if (root == NULL) {
		cout << " Empty List " << endl;
	}
}
template<class T>
void Tree<T>::insert(const T &el) {
	Node<T> *p = root, *prev = 0;
	while (p != 0) {
		prev = p;
		if (p->key < el)
			p = p->right;
		else
			p = p->left;
	}
	if (root == 0)
		root = new Node<T>(el);
	else if (prev->key<el)
		prev->right = new Node<T>(el);
	else
		prev->left = new Node<T>(el);
}

template<class T>
void Tree<T>::deleteNode(Node<T> *&node) {
	Node<T> *prev, *tmp = node;
	if (node->right == 0)
		node = node->left;
	else if (node->left == 0)
		node = node->right;
	else {
		tmp = node->left;
		prev = node;
		while (tmp->right != 0) {
			prev = tmp;
			tmp = tmp->right;
		}
		node->key = tmp->key;
		if (prev == node)
			prev->left = tmp->left;
		else prev->right = tmp->left;
	}
	delete tmp;
}

template<class T>
void Tree<T>::showinorder(Node<T> *p ) {

	if (root != NULL)
	{
		if (p->left != NULL) {
			showinorder(p->left);
		}
		cout << p->key << " " << endl ;
		if (p->right != NULL) {
			showinorder(p->right);
			
		}
	}
	else if (root == NULL) {
		cout << " Empty List " << endl;
	}

}
#endif // Binary_Search_Tree#pragma once
