#include <iostream>
#include <string>
#include<list>
#include<fstream>
#include "tree.h"
#include<sstream>

using namespace std;

void main()
{
	// var declearation
	Tree<string> mytree;
	string str;
	string buf; // buffer string
	ifstream myText;
	string completeLine;
	int line = 1; 

	// push words into the tree
	myText.open("text.txt");//open text file 
	while (getline(myText, str)) 
	{
		stringstream ss(str); // insert the string into stream
		while (ss >> buf)
		{		
			completeLine =  buf+" " + to_string(line); // combine line num and text 
			mytree.insert(completeLine);
		}
		line++;
	}
	
	mytree.inorder(); // display
	
	system("pause");
}